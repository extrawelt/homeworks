/*our services Tabs section*/

let tabscontent = document.getElementsByClassName("tabcontent");

window.onload = function () {
    document.querySelector(".tab").addEventListener("click", openTabs); //catch click on div.tab area

    function openTabs(event) {

        let attr_value = event.target.getAttribute("data-tab"); //get current value of data-set
        let clicked_area = event.target;
        if (clicked_area.classList == "tablink") {
            let activetabs = document.getElementsByClassName("tablink");
            for (let i = 0; i < activetabs.length; i++) {
                activetabs[i].classList.remove("active");
            }
            clicked_area.classList.add("active");
            for (let i = 0; i < tabscontent.length; i++) {
                if (attr_value == i) { /*compare current data attr value with index of content collection tab*/

                    tabscontent[i].style.display = "block";
                } else {
                    tabscontent[i].style.display = "none";
                }
            }
        }
    }
}



/*amazing work section*/

let img_collection = document.getElementsByClassName("with_image"); //collection  of divs.with images

let button_to_be_clicked = document.getElementById("button_to_load_more"); //load more button 

function toHide() { //to hide extra 12 img on load 
    for (let i = 12; i < img_collection.length; i++) {
        img_collection[i].style.display = "none";
    }
}

toHide();

function toLoadMoreImg() {
    for (let i = 12; i < img_collection.length; i++) { //toLoad hidden images 
        img_collection[i].style.display = "block";
    }
    button_to_be_clicked.style.display = "none"; //to hide button
}


let div_with_buttons = document.querySelector(".with-tabs");

div_with_buttons.addEventListener("click", toSortImg); /*catch the click on the area with tabs and call the function*/

function toSortImg(event) {
    if (event.target.classList == "tabs") { /*if the click on the class .tabs*/

        let current = event.target.getAttribute("data-image_category"); /*get the data-set attr value*/
        //let pics_collection = document.getElementsByClassName("with_image"); /*get collction of divs with images*/

        let dynamic_div = document.querySelector(".pictures_container");

        for (let i = 0; i < img_collection.length; i++) {
            img_collection[i].style.display = "block";
        }
        for (let i = 0; i < img_collection.length; i++) {
            if (current == "all_pics") { //first tab - show all pics
                toHide();
            } else if (current != img_collection[i].getAttribute("data-image_category")) { /*if current button.tabs attr value doesn't match the div with image attr*/

                dynamic_div.style.width = "auto"; //to make general div more flexible
                dynamic_div.style.justifyContent = "center";
                img_collection[i].style.display = "none"; //to hide mismatched divs
            }
        }
    }
}


/*What people say abt theHam*/

let next_btn = document.getElementById("next");
let prev_btn = document.getElementById("previous");

let descriptionCollect = document.getElementsByClassName("individual-description"); //get the collection of person's description
let imgCollection = document.getElementsByClassName("avatar"); //collection of big images in circle 

let smallImgCollection = document.getElementsByClassName("slider-pics"); //collection of small images in the bottom 

let currentSlide = 0;
let currentPersonDescription = 0;


function toNextSlide(n) {
    for (let i = 0; i < smallImgCollection.length; i++) { /*remove class .active for small avatars in slider*/
        smallImgCollection[i].classList.remove("active-slider-picture");
    }
    imgCollection[currentSlide].style.display = "none";
    currentSlide = (n + imgCollection.length) % imgCollection.length;
    imgCollection[currentSlide].style.display = "block";
    smallImgCollection[currentSlide].classList.add("active-slider-picture");
}

function toChooseDescription(n) { //go to the next/prev slide of description 
    descriptionCollect[currentPersonDescription].style.display = "none";
    currentPersonDescription = (n + descriptionCollect.length) % descriptionCollect.length;
    descriptionCollect[currentPersonDescription].style.display = "block";
}

next_btn.onclick = function () { //next btn click 
    toChooseDescription(currentPersonDescription + 1); //next description field
    toNextSlide(currentSlide + 1); //next big image in circle 
}


prev_btn.addEventListener("click", function () {
    if (currentSlide > 0) {
        toNextSlide(currentSlide - 1);
    } else {
        for (let i = 0; i < smallImgCollection.length; i++) {
            smallImgCollection[i].classList.remove("active-slider-picture");
        }
        currentSlide = imgCollection.length - 1; /*if current slide has [0] position, assign value  of the last slide index*/
        imgCollection[currentSlide].style.display = "block";
        smallImgCollection[currentSlide].classList.add("active-slider-picture");
        for (let i = 0; i < imgCollection.length - 1; i++) {
            imgCollection[i].style.display = "none";
        }
    }
})

prev_btn.addEventListener("click", function () {
    if (currentPersonDescription > 0) {
        toChooseDescription(currentPersonDescription - 1);
    } else {
        currentPersonDescription = descriptionCollect.length - 1;
        descriptionCollect[currentPersonDescription].style.display = "block";
        for (let i = 0; i < descriptionCollect.length - 1; i++) {
            descriptionCollect[i].style.display = "none";
        }
    }
})

let sliderImgLine = document.querySelector(".slider-line");

sliderImgLine.addEventListener("click", toChooseSmallImg);


function toChooseSmallImg(event) {   //direct mouseclick on small img in slider

    let currentClickedArea = event.target;
    let currentImgAttr = currentClickedArea.getAttribute("data-person");

    if (currentClickedArea.classList == "slider-pics") {
        for (let i = 0; i < smallImgCollection.length; i++) {
            if (currentImgAttr == i) {
                toNextSlide(i);
                toChooseDescription(i);
            }
        }
    }
}



/*Gallery of best images*/

$(document).ready(function () {
    $('.load-more-in-gallery').click(function () {
        $('.hidden').show(2000);
        $('.hidden').css('display', 'inline-flex');
        $('.load-more-in-gallery').hide();

    });

});


$(document).ready(function () {
    $('.wrapper').hover(function () {

        let current_width = $(this).width();
        let current_height = $(this).height();
        $('.hovered').css({
            "height": current_height,
            "width": current_width
        });
    });
});
