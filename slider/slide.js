let prev = document.getElementById("prev");
let next = document.getElementById("next");

let img = document.getElementsByClassName("pictures");
console.log(img);


let current_slide = 0;

function toNext(n) {
    img[current_slide].style.display = "none";
    current_slide = (n + img.length) % img.length;
    img[current_slide].style.display = "block";
}

next.onclick = function () {
    toNext(current_slide + 1);
}

prev.onclick = function () {
    if (current_slide > 0) {
        toNext(current_slide - 1);
    } else {
        current_slide = img.length - 1;
        img[current_slide].style.display = "block";
        for (let i = 0; i < img.length - 1; i++) {
            img[i].style.display = "none";
        }
    }
}
