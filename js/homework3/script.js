/*function factorial(a) {
    let result = 1;
    for (let i = 2; i <= a; i++) {
        result *= i;
        console.log(result);
    }
}
Factorial(8);
*/

function recursFactorial(a) {
    if (a != 1) {
        return a * recursFactorial(a - 1);
    } else {
        return 1;
    }
}

let result = +prompt("Enter your number");

console.log(recursFactorial(result));