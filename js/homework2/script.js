function isSimple(a) {
    let divider = 2;
    let result = Math.sqrt(a);
    while (divider <= Math.round(result)) {
        if (a % divider != 0) {
            divider++;
        } else {
            return false;
        }
    }
    return true;
}

let min = +prompt("Enter the min number", 2);
let max = +prompt("Enter the max number");
let arraySimple = [];
let arrayNotSimple = [];
for (min; min <= max; min++) {
    if (isSimple(min)) {
        arraySimple.push(min);
    } else {
        arrayNotSimple.push(min);
}
}
console.log("simple numbers " + arraySimple);
console.log("not simple numbers " + arrayNotSimple);

